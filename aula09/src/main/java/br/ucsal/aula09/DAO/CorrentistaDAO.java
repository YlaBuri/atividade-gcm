package br.ucsal.aula09.DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.aula09.DBUtil;
import br.ucsal.aula09.domain.Correntista;

public class CorrentistaDAO {
	
	private static final String QUERY_FIND_ALL = "select id, nm, nu_telefone, nu_ano_nascimento from correntista order by nm asc";

	private CorrentistaDAO() {
	}

	public static List<Correntista> findAll() throws SQLException {
		List<Correntista> correntistas = new ArrayList<>();

		try (Statement stmt = DBUtil.getConnection().createStatement();
				ResultSet rs = stmt.executeQuery(QUERY_FIND_ALL)) {
			while (rs.next()) {
				correntistas.add(resultSet2Correntista(rs));
			}
		}
		String texto = "Yla � demais";
		String texto2 = "Yla � demais";
		return correntistas;
	}

	private static Correntista resultSet2Correntista(ResultSet rs) throws SQLException {
		Integer id = rs.getInt("id");
		String nome = rs.getString("nm");
		String telefone = rs.getString("nu_telefone");
		Integer anoNascimento = rs.getInt("nu_ano_nascimento");
		return new Correntista(id, nome, telefone, anoNascimento);
	}

}
