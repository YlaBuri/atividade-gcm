package br.ucsal.aula09;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	private static final String URL = "jdbc:postgresql://localhost:5432/aula09";

	private static Connection connection;

	private DBUtil() {
	}

	public static void connect(String user, String password) throws SQLException {
		connection = DriverManager.getConnection(URL, user, password);
	}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			throw new SQLException("Conex�o n�o dispon�vel.");
		}
		return connection;
	}

	public static void closeConnection() throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}
}
