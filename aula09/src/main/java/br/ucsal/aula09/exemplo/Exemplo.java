package br.ucsal.aula09.exemplo;

import java.sql.SQLException;
import java.util.List;

import br.ucsal.aula09.DBUtil;
import br.ucsal.aula09.DAO.CorrentistaDAO;
import br.ucsal.aula09.domain.Correntista;

public class Exemplo {
	public static void main(String[] args) throws SQLException {
		try {
			DBUtil.connect("postgres", "abcd1234");
			listarCorrentistas();
		} finally {
			DBUtil.closeConnection();
		}
	}

	private static void listarCorrentistas() throws SQLException {
		List<Correntista> correntistas = CorrentistaDAO.findAll();
		System.out.println("Correntistas: ");
		correntistas.stream().forEach(System.out::println);
	}

}
